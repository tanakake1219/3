//  関数宣言文
function 面積(height, width){
    return height * width;
}

console.log(面積(5,6));


//関数式
const 点数 = function(kokugo, sugaku) {
    return kokugo + sugaku;
};

console.log(点数(80, 27));

//Arrow Function
const taro = {
    id:"1",
    name:{
        first: "taro",
        last: "yamada",
    }
};

const mizuki = {
    id:"2",
    name:{
        first: "mizuki",
        last: "takeshita",
    }
};

const itiro = {
    id:"3",
    name:{
        first: "itiro",
        last: "tanaka",
    }
};

const Name = (person) => {
    return "[" + person.id + "]" + person.name.first + " " + person.name.last;
}

console.log(Name(taro));
console.log(Name(mizuki));
console.log(Name(itiro));
